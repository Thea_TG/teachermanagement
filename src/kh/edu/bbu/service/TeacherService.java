package kh.edu.bbu.service;

import kh.edu.bbu.models.Teacher;

import java.util.List;

public interface TeacherService {
    void addNeTeacher(Teacher teacher);
    void updateTeacher(Teacher teacher);
    void deleteTeacher(String id);
    void FindById(String id);
    List<Teacher>getAllTeacher();
    void getAllTeacherWithTableGenerator(List<Teacher> list);
    void menuOption();

}
