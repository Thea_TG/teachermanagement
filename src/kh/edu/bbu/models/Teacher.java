package kh.edu.bbu.models;

public class Teacher {
    // Data Teacher
    private String id;
    private String firstName;
    private String lastName;
    private String gender;
    private String phone;
    private String address;
    private String dateOfBirth;
    private String University;
    private String ClassAmount;
    private String TeachPeriod;
    private String Subject;

    public Teacher() {
    }

    // Constructor
    public Teacher(String id, String firstName, String lastName, String gender, String phone, String address, String dateOfBirth, String university, String classAmount, String teachPeriod, String subject) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.phone = phone;
        this.address = address;
        this.dateOfBirth = dateOfBirth;
        this.University = university;
        this.ClassAmount = classAmount;
        this.TeachPeriod = teachPeriod;
        this.Subject = subject;
    }

    // Getter and Setter
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getUniversity() {
        return University;
    }

    public void setUniversity(String university) {
        University = university;
    }

    public String getClassAmount() {
        return ClassAmount;
    }

    public void setClassAmount(String classAmount) {
        ClassAmount = classAmount;
    }

    public String getTeachPeriod() {
        return TeachPeriod;
    }

    public void setTeachPeriod(String teachPeriod) {
        TeachPeriod = teachPeriod;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    // ToString
    @Override
    public String toString() {
        return "Teacher{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", University='" + University + '\'' +
                ", ClassAmount='" + ClassAmount + '\'' +
                ", TeachPeriod='" + TeachPeriod + '\'' +
                ", Subject='" + Subject + '\'' +
                '}';
    }

}
